
#### Method Functions _____________________________________________________________________

	- Method Functions can be used to use functions as fields in a struct. 
	  The <method> function affilated with a type. it has a similar syntax as function except
	  after the <func> keywork has a paramter called the <receiver>, which the main body of
	  that method. 

** Syntax of a Method: 

	func (r ReceiverType) funcName(paramters) (results) 

	EX.1 Rectangle without Method

		package main

		import "fmt"

		type Rectangle struct {
			width, height float64
		}

		func area(r Rectangle) float64 {
			return r.width * r.height
		}

		func main() {
			r1 := Rectangle{12, 2}
			r2 := Rectangle{9, 4}
			fmt.Println("Area of r1 is: ", area(r1))
			fmt.Println("Area of r2 is: ", area(r2))
		}

	EX.1-A 
		- In the above function we create a function called <area> but it's not a method
		  of the rectangle struct. The function and struct are two independent things. 

	EX.2 Method Example

		package main

		import (
			"fmt"
			"math"
		)

		type Circle struct {
			radius float64
		}

		type Rectangel struct {
			width, height float64
		}

		// method 
		func (c Cirlce) Area() float64 {
			return c.radius * c.radius * math.Pi
		}

		// method 
		func (r Rectangle) Area() float64 {
			return r.witdh * r.height
		}

		func main() {
			c1 := Circle{10}
			c2 := Circle{25}
			r1 := Rectangle{9, 4}
			r2 := Rectangle{12, 2}

			fmt.Println("Area of c1 is: ", c1.Area())
			fmt.Println("Area of c2 is: ", c2.Area())
			fmt.Println("Area of r1 is: ", r1.Area())
			fmt.Println("Area of r2 is: ", r2.Area())
		}

*** Notes for how to use a Method

	1) If the name of the method are the same but they don't share the same recievers, they
	   are not the same
	2) Methods are able to access fields within receivers
	3) Use <.> (dot) to call a method in the struct, the same way fields are called. 

	Use the following formate to define a customized type: 

		type typeName typeLiteral

	EX.3 Customized Types: 

		type age int
		type money float32
		type month map[string]int

		m := months {
			"January":31, 
			"February":28, 
			...				// receiver passed by value, NOT by reference
			"December":31,
		}

	EX.4 You can use as many methods in custom types as you want

		package main 

		import "fmt"

		const (
			WHITE = iota 
			BLACK
			BLUE
			RED
			YELLOW
		)

		type Box struct {
			width, height, depth float64
			color Color
		}

		type Color byte
		type BoxList []Box // a slice of boxes

		// method 
		func (b Box) Volume() float64 {
			return b.width * b.height * b.depth
		}

		// method with a pointer reciever 
		func (b *Box) SetColor(c Color) {
			b.color = c
		}

		// method 
		func (bl BoxList) BiggestColor() Color {
			v := 0.00
			k := Color(WHITE)
			for _, b := range bl {
				if b.Volume() > v {
					v = b.Volume()
					k = b.color
				}
			}
			return k
		}

		// method
		func (bl Boxlist) PaintItBlack() {
			for i, _ := range bl {
				bl[i].SetColor(BLACK)
			}
		}

		// method 
		func (c Color) String() string {
			strings := []string{"WHITE", "BLACK", "BLUE", "RED", "YELLOW"}
			return strings[c]
		}

		func main() {
			boxes := BoxList {
				Box{4, 4, 4, RED},
	        	Box{10, 10, 1, YELLOW},
				Box{1, 1, 20, BLACK},
				Box{10, 10, 1, BLUE},
				Box{10, 30, 1, WHITE},
				Box{20, 20, 20, YELLOW},
			}

			fmt.Printf("We have %d boxes in our set\n", len(boxes))
			fmt.Println("The volume of the first one is", boxes[0].Volume(), "cm3")
			fmt.Println("The color of the last one is", boxes[len(boxes)-1].color.String())
			fmt.Println("The biggest one is", boxes.BiggestsColor().String())

			// paint all boxes black
			boxes.PaintItBlack()

			fmt.Println("The color of the second one is", boxes[1].color.String())
			fmt.Println("Obviously, now, the biggest one is", boxes.BiggestsColor().String())
		}

	EX.4-A Explination

		- Use <Color> as alias of <byte>
		- Define a struct <Box> which has fields height, width, length, and color
		- Define a struct BoxList which has <Box> as its field
	  > Defined Methods for Customized Types. 
	  	- <Volume()> uses Box as its reciever and returns the volume of Box. 
		- <SetColor(c Color)> changes Box's color
		- <BiggestsColor()> returns the color which has the biggest volume
		- <PaintItBlack()> sets color for all Box in BoxList to black
		- <String()> uses Color as its receiver, returns the string format of color name

*** Pointer as Receiver **

	In the above example <*Box> is used as a receiver. You must use a pointer to change
	Box's color in this method. If you don't use a pointer, it will only change the 
	value inside a copy of Box. 



























