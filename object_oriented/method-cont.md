## Inheritance / Overriding _______________________________________________________________

	- Method Inheritance: If an anonymous field has methods, then the struct that contains
	                      the field will have all the methods from it as well. 

	EX.1 Inheritance of Method

		package main

		import "fmt"

		type Human struct {
			name string
			age int
			phone string
		}

		type Student struct {
			Human // anonymous field
			school string
		}

		type Employee struct {
			Human
			comapny string 
		}

		// define a method in Human
		func (h *Human) SayHi() {
			fmt.Printf("Hi, I am %s you can call me on %s\n", h.name, h.phone) 
		}

		func main() {
			sam := Employee{Human{"Sam", 45, "111-111-1111"}, "Golang Inc"}
			mark := Student{Human{"Mark", 24, "222-222-2222"}, "MIT"}

			sam.SayHi()
			mark.SayHi()
		}


	- Method Overriding: If we want employee to have its own method <SayHi>, we can define
	  a method that has the same name in Employee, and it will hide <SayHi> in Human when
	  we call it. 

	EX.2 Method Overriding

		package main

		import "fmt"

		type Human struct {
			name string
			age int
			phone string
		}

		type Student struct {
			Human
			school string
		}

		type Employee struct {
			Human 
			company string 
		}

		func (h *Human) SayHi() {
			fmt.Printf("Hi, I am %s you can call me %s\n", h.name, h.phone)
		}

		func (e *Employee) SayHi() {
			fmt.Printf("Hi I am %s, I work at %s. Call me on %s\n", e.name, 
			    e.company, e.phone) // yes you can split into 2 lines
		}

		func main() {
			sam := Employee{Human{"Sam", 45, "111-111-1111"}, "GoLang Inc"}
			mark := Student{Human{"Mark", 25, "222-222-2222"}, "MIT"}

			sam.SayHi()
			mark.SayHi()
		}


	













































