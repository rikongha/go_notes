
## Control Statements and Functions 

	- There are three categories of flow control: 
		1) Conditional
		2) Cycle Control
		3) Unconditional Jump



#### If Statements __________________________________________________________________________________________

	- <if> will most likely be the most common
	- <if> statements in go don't need parentheses

	EX.1
		if x > 10 {
			fmt.Printf("x is greater than 10")
		} else {
			fmt.Printf("x is less than or equal to 10")
		}

	- The scope of the variable defined in an <if> statement can be used inside of the statement and are only
	  available inside the block of the defining <if>. 

	EX.2
		// initialize x, then check if x greater than
		if x := computedValue(): x > 10 {
			fmt.Printf("x is greater than 10")
		} else {
			fmt.Printf("x is less than 10")
		} 

		// the following code will not compile
		fmt.Printf(x) 

	EX.3 If-Else for multiple conditions
		if integer == 3 {
			fmt.Printf("The integer is equal to 3")
		} else if integer < 3 {
			fmt.Printf("The integer is less than 3")
		} else {
			fmt.Printf("The integer is greater than 3")
		} 

#### GoTo Statements ______________________________________________________________________________________

	- <goto> keyword must be used with caution, <goto> reroutes the control flow to a previously defined 
	  label within the body of the same code block. 
	- The label name is case sensitive 

	EX.1 
		func myFunc() {
			i := 0 
		Here:			// label ends with ":"
			fmt.Println(i) 
			i++
			goto Here	// jump to label "Here" <- case sensitive 

#### For Statements _______________________________________________________________________________________

	- <for> is the most powerful control logice in Go. <for> can read data in loops and iterative
	  operations, just like <while> 

	EX.1 
		for expression1; expression2; expression3 {
			// expression1 & exp3 are variable definitions or return values from functions, and expression2
			// is a conditional statement. expression1 will be executed once before looping, and 
			// expression3 will be executed after each loop. 
		}

	EX.2 For expression base iteration example 
		package main 

		import "fmt"

		func main(){
			sum := 0
			for index:=0; index < 10; index++ {
				sum += index 
			}
			fmt.Printf("sum is equal to ", sum)
		} 
		// Print: sum is equal to 45

	- Sometimes you may need to use multiple assignments, but Go doesn't have the <,> operator, so we use
	  parallel assignment like <i, j = i + 1, j - 1> 
	
	EX.3 -> EX.2 omition syntax if an expression isn't necessary 
		sum := 1 
		for ; sum < 1000; {
			sum += sum 
		} 

	EX.4 -> EX.3 Omiting <;> is okay as well. 
		sum := 1 
		for sum < 1000 {
			sum += sum 
		}

	- There are two important operations in loops which are
		1) break
			a. <break> jumps out of the loop
		2) continue
			a. <continue> skips the current loops and starts the next one. 
	- If you have nested loops, use <break> along with labels

	EX.5 
		for index := 10; index > 0; index-- {
			if index == 5{
				break		// or contine
			}
			fmt.Printf(index)
		}

		// break prints 10, 9, 8, 7, 6
		// continue would print 10, 9, 8, 7, 6, 5, 4, 3, 2, 1

	- <for> can read data from <array>, <slice>, <map>, and <string> when it is used together with <range> 
	EX.6 
		for k, v := range map {
			fmt.Printf("map's key: ", k)
			fmt.Printf("map's val: ", v) 
		} 

	EX.7 Underslash value discard
		- Because go supports multi-value returns and all values must be used or it won't compile. You can use
	  	  <_> (underslash) to discard certain return values.

	Code: 
		for _, v := range map {
			fmt.Printf("map's val: ", v)
		}

	EX.8 
		- You can create an infinite loop !!!(BAD)!!! which would be equivalent to <while true { . . . }> 
	
	Code:
		for {
			// logic here
		} 





























