
#### Defer Function ____________________________________________________________________________

	- <defer> 
	- You can have many <defer> statements in one function; they will execute in reverse order 
	  when the program executes to the end of functions. 
	- In the case where the programs opens some resource files, these files would have to be 
	  closed before the function can return with errors. 

	EX.1 File Open

		func ReadWrite() bool {
			file.Open("file") 
			// Do some work 
			if falureX {
				file.Close()
				return false
			} 

			if failureY {
				file.Close() 
				return false
			} 

			file.Close() 
			return true
		}

	EX.1-A
		- Code is being repeated several times. <defer> solves this problem very well. 

	EX.2 File Open with Defer 

		func ReadWrite() bool {
			file.Open("file")
			defer file.Close()
			
			if failureX {
				return false
			}

			if failureY {
				return false
			}

			return true
		}

	EX.3 Defer's execute by reverse order

		for i := 0; i < 5; i++ {
			defer fmt.Printf("%d ", i) 
		} 	// returns <4 3 2 1 0> 















