
#### Switch Functions ____________________________________________________________________________


	- If you find that you're using to many <if-else> loop breaks, then you can use a switch
	  statement. 

	EX.1 Base Switch Statement 

		switch sExpr {
		case expr1:
			// code here
		case expr2: 
			// code here
		case expr3:
			// code here 
		default:
			// other code 
		}

	EX.1-A: The type of expression for the switch statement must be the same type <bool, int, ect>
			This makes the switch statements highly flexable. The statement executes from top
			to bottom, until conditional matching happens. 

	EX.2 Switch Statement

		i := 10
		switch i {
		case 1: 
			fmt.Printf("i is equal to 1")
		case 2, 3, 4:
			fmt.Printf("i is equal to 2, 3, or 4")
		case 10: 
			fmt.Printf("i is equal to 10")
		default:
			fmt.Printf("All I know is that i is an integer")
		} 

	EX.2-A: In the second case statement, you can put many values in one <case>, and you don't
			need to add the break keyword at the end of the <case>'s body. It will jump out of
			the switch body once it matched any case. If you want to continue matching more cases
			you need to use the <fallthrough> statement. 

	EX.3 Fallthrough Statements

		integer := 6
		switch integer {
		case 4:
			fmt.Println("integer <= 4")
			fallthrough
		case 5:
			fmt.Println("integer <= 5")
			fallthrough
		case 6:
			fmt.Println("integer <= 6")
			fallthrough
		case 7:
			fmt.Println("integer <= 7")
			fallthrough
		case 8:
			fmt.Println("integer <= 8")
			fallthrough
		default:
			fmt.Println("default case")
		}






































