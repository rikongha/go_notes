
#### Functions as Values and Types _____________________________________________________________

	- Functions are also variables in GO, we can use <type> to define them. Functions that have
	  the same signautre can be seen as the same type. 

	EX.1 Function as Type Syntax 

**	type typeName func(input1 inputType1, input2 inputType2 [, ...]) (result1 resultType1 [, ...])


	EX.1-A 
		- This allows us to pass functions as values 

	EX.2 

		package main 

		import "fmt"

		type testInt func(int) bool 	// define a function type of variable 

		func isOdd(integer int) bool {
			return integer%2 != 0 
		} 

		func isEven(integer int) bool {
			return integer%2 == 0 
		}

		// pass the function `f` as an argument to another function 
		func filter(slice []int, f testInt) []int {
			var result []int 
			for _, value := range slice {
				if f(value) {
					result = append(result, value) 
				} 
			} 
			return result 
		}

		var slice = []int{1, 2, 3, 4, 5, 7} 

		func main() {
			odd := filter(slice, isOdd) 
			even := filter(slice, isEven) 

			fmt.Println("slice = ", slice)
			fmt.Println("Odd elements of slice are: ", odd) 
			fmt.Println("Even elements of slice are: ", even) 
		} 

	EX.2-A 
		- <testInt> is a variable that has a function as type and the returned values and
		  arguments of <filter> are the same as those of <testInt>. Therefore, we can have 
		  complex logic, while maintaining flexibility in the code. 

	

