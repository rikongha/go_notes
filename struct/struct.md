
#### Struct _________________________________________________________________________________

	- You can create containers of other properties or fields in GO. These are called <struct>
	  with <struct> you can create different types to represent different fields. 

	EX.1

		type person struct {
			name string
			age int
		}

		- Created a type called person which has a name as a <string> and age as an <int> 

	EX.2 Using Struct's

		type person struct {
			name string 
			age int 
		}

		var P person		// p is person type 

		P.name = "Astaxie" 	// assign "astaxie" to the field 'name' of <P> 
		P.age = 25			// assign 25 to field 'age' of <P> 
		fmt.Printf("The person's name is %s\n", P.name) // access field 'name' of <P>

	- There are three main ways to declare a <struct> 
	
	1) Assign inital values by order 

		P := person{"Tom", 25} 

	2) Use the format <field:value> to initalize the struct without order

		P := person{age:24, name:"Bob"}

	3) Define an anonymous struct, then initialize it 

		P := struct{name string; age int}{"Amy", 18} 

	
	EX.3 Full Struct Example 

		package main 

		import (
			"fmt"
		) 

		// define a new type 
		type person struct {
			name string
			age int
		}

		// struct is passed by value
		// compare the age of two people, then return the older person and diff of age
		func Older(p1, p2 person) (person, int) {
			if p1.age > p2.age {
				return p1, p1.age - p2.age
			}
			return p2, p2.age - p1.age
		}

		func main() {
			var tom person 

			tom.name, tom.age = "Tom", 18
			bob := person{age: 25, name: "Bob"}
			paul := person{"Paul", 43}

			tb_Older, tb_diff := Older(tom, bob) 
			tp_Older, tp_diff := Older(tom, paul)
			bp_Older, pb_diff := Older(bob, paul)

			fmt.Printf("Of %s and %s, %s is older by %d years\n", tom.name, bob.name, tb_Older.name, tb_diff)
			fmt.Printf("Of %s and %s, %s is older by %d years\n", tom.name, paul.name, tp_Older.name, tp_diff)
			fmt.Printf("Of %s and %s, %s is older by %d years\n", bob.name, paul.name, bp_Older.name, bp_diff)

		}













































