
#### Embedded Fields in Struct ______________________________________________________________

	- Go supports fields without names, but with types. They're called embedded fields. 
	  When the embedded field is a struct, all the fields in that struct will implicity
	  be the fields in the struct in which it has been embedded. 

	EX.1

		package main

		import "fmt"

		type Human struct {
			name string
			age int
			weight int
		}

		type Student struct {
			Human	// embedded field, it means student struct includes all fields in <Human>
			specialty string 
		}

		func main() {
			// instantiate and initialize a student
			mark := Student{Human{"Mark", 25, 120}, "Computer Science"} 

			// access fields
			fmt.Println("His name is ", mark.name)
			fmt.Println("His age is ", mark.age)
			fmt.Println("His weight is ", mark.weight)
			fmt.Println("His specialty is ", mark.specialty)

			// modify mark's speciality 
			mark.specialty = "AI"
			fmt.Println("Mark changed his specialty)
			fmt.Println("His specialty is ", mark.specialty)

			fmt.Println("Mark became old. He is not an athlete anymore")
			mark.age = 46
			mark.weight += 60
			fmt.Println("His age is ", mark.age)
			fmt.Println("His weight is ", mark.weight)
		}

	- You can use <struct> to access the embedded field from within another field

	EX.2 Cont. from EX.1 

		mark.Human = Human{"Marcus", 55, 220}
		mark.Human.age -= 1 	// accessing the embedded field from student -> human.age

	EX.3 All types can be used as embedded fields 

		package main

		import "fmt"

		type Skills []string 

		type Human struct {
			name string
			age int
			weight int
		}

		type Student struct {
			Human			// struct as embedded field
			Skills			// string slice as embedded field
			int				// built-in type as embedded field 
			specialty string
		}

		func main() {
			// initialize student Jane
			jane := Student{Human: Human{"Jane", 35, 100}, specialty: "biology"}
			// access fields
		    fmt.Println("Her name is ", jane.name)
	   		fmt.Println("Her age is ", jane.age)
		    fmt.Println("Her weight is ", jane.weight)
			fmt.Println("Her specialty is ", jane.specialty)
			// modify value of skill field 
			jane.Skills = []string{"anatomy"}
			fmt.Println("Her skills are ", jane.Skills)
			fmt.Println("She acquired two new ones ")
			jane.Skills = append(jane.Skills, "physics", "golang") 
			fmt.Println("Her skills are now ", jane.Skills)
			// modify embedded fields
			jane.int = 3
			fmt.Println("her preferred number is ", jane.int) 
		}

	EX.3-A
		- In the above example, you can see that all types can be embedded fields and we
		  can use functions to operate on them. 

	- *PROBLEM* What happens when you have two fields that have the same name? 
		Answer: The outer fields get upper access levels, which means when you access
				<name1.type1>, we will get the field called <type1> in <name1>, and not
				the top level struct. This feature can be simply seen as field <overload>ing

	EX.4 Duel Name

		package main

		import "fmt"

		type Human struct {
			name string
			age int
			phone string // Human has phone field
		}

		type Employee struct {
			Human
			specialty string
			phone string // phone in employee 
		}

		func main() {
			Bob := Employee{Human{"Bob", 34, "777-44-XXXX"}, "Designer", "333-222"} 

			fmt.Println("Bob's work phone is: ", Bob.phone)
			fmt.Println("Bob's personal Phone is: ", Bob.Human.phone)
		}





























