#### RPC ________________________________________________________________________________

	- RPC was intended to be the function call mode for networked systems. Clients execute
	  RPCs like they call native funcitons, except they package the function paramters, 
	  and send them through the network to the server. 

	- RPC = remote procedure call

	- RPC: has three levels of standard library TCP, HTTP, and JSON RPC. Go RPC is not 
	  like other traditional RPC systems, it requires you to use GO applications on both
	  client and server sides because it encodes content using <Gob>. 

	- Funciton of Go RPC must abide by the following rules fo remote access, otherwire 
	  the corresponding calls will be ignored. 
	  	1) functions are exported (capitalized)
		2) Functions must have two arguments with exported types
		3) the first argument is for receiving from teh client, and the second one has 
		   to be a pointer and is for replying to the client
		4) functions must has a return value of error type


	EX.1 RPC function call 

		func (t *T) MethodName(argType T1, replyType *T2) error 

	EX.1-A
		- Where T, T1, and T2 must be able to be encoded by the package/gob package. 
		- Any kind of RPC has to go through a network to transfer data. Go RPC can either
		  use HTTP or TCP. The benefits of using HTTP is that you can reuse some fucntions
		  from the net/http package. 

	EX.2 HTTP RPC Server Side Code 

		package main 

		import (
			"errors"
			"fmt"
			"net/http"
			"net/rpc"
		)

		type Args struct {
			A, B int
		}

		type Quotient Struct {
			Quo, Rem int 
		}

		type Arith int

		func (t *Arith) Multiply(args *Args, reply *int) error {
			*reply = args.A * args.B
			return nil 
		}

		func (t *Arith) Divide(args *Args, quo *Quotient) error {
			if args.B == 0 {
				return errors.New("divide by zero")
			}
			quo.Quo = args.A / args.B
			quo.Rem = args.A % args.B
			return nil 
		}

		func main() {

			arith := new(Arith)
			rpc.Register(arith)
			rpc.HandleHTTP()

			err := http.ListenAndServer(":1234", nil)
			if err != nil {
				fmt.Println(err.Error())
			}
		}

	EX.2-A
		- We register a RPC service of Arith, then reigstered this service on HTTP through
		  <rpc.HandleHTTP>, after that, we are able to transfer data through HTTP

	EX.3 Client Side Code for EX.2

		package main 

		import (
			"fmt"
			"log"
			"net/rpc"
			"os"
		)

		type Args struct {
			A, B int
		}

		type Quotient Struct {
			Quo, Rem int
		}

		func main() {
			if len(os.Args) != 2 {
				fmt.Println("Usage: ", os.Args[0], "server")
				os.Exit(1)
			}
			serverAddress := os.Args[1]

			client, err := rpc.DialHTTP("tcp", serverAddress+":1234")
			if err != nil {
				log.Fatal("dialing:", err)
			}
			// Synchronous Call 
			args := Args{17, 8}
			var reply int 
			err = client.Call("arith.Multiply", args, &reply)
			if err != nil {
				log.Fatal("arith error:", err)
			}
			fmt.Printf("Arith: %d*%d=%d\n", args.A, args.B, reply)

			var quot Quotient
			err = client.Call("Arith.Divide", args, &quot)
			if err != nil {
				log.Fatal("Arith error:", err)
			}
			fmt.Printf("Arith: %d/%d=%d remainder %d\n", args.A, args.B, quot.Quo, quot.Rem)

		}

	EX.3-A
		- Compile the client and server side code separately then start the server and
		  client. You'll then have something similar as follows after you input some data. 


***TCP RPC***

	EX.4 TPC RPC Server Side Code

		pacakge main 

		import ( 
			"errors"
			"fmt"
			"net"
			"net/rpc"
			"os"
		)

		type Args struct {
			A, B int
		}

		type Quotient struct {
			Quo, Rem int
		}

		type Arith int

		func (t *Arith) Multiply(args *Args, reply *int) error {
			*reply = args.A * args.B
			return nil 
		}

		func (t *Arith) Divide(args *Args, quo *Quotient) error {
			if args.B == 0 {
				return errors.New("divide by zero")
			}

			quo.Quo = args.A / args.B
			quo.Rem = args.A % args.B
			return nil 
		}

		func main() {

			arith := new(Arith)
			rpc.Register(arith)

			tcpAddr, err := net.ResolveTCPAddr("tcp", ":1234")
			checkError(err) // function below

			for {
				conn, err := listener.Accept()
				if err != nil {
					continue
				}
				rpc.ServeConn(conn)
			}
		}

		func checkError(err Error) {
			if err != nil {
				fmt.Println("Fatal error: ", err.Error())
				os.Exit(1)
			}
		}

	EX.4-A
		- The difference between HTTP RPC and RCP RPC is taht we have to control connections
		  by ourselves if we use TCP RPC, then pass connections to RPC for processing. 
		- This is a blocking pattern, use goroutines to extend this application. 

	EX.5 TCP RPC Client Side

		package main

		import (
			"fmt"
			"log"
			"net/rpc"
			"os"
		)

		type Args struct {
			A, B int 
		}

		type Quotient struct {
			Quo, Rem int 
		}

		func main() {
			if len(os.Aregs) != 2 {
				fmt.Println("Usage: ", os.Args[0], "server:port")
				os.Exit(1)
			}

			service := os.Args[1]

			client, err := rpc.Dial("tcp", service)
			if err != nil {
				log.Fatal("dialing:", err)
			}

			// synchronous call 
			args := Args{17, 8}
			var reply int 
			err = client.Call("Arith.Multiply", args, &reply)
			if err != nil {
				log.Fatal("arith error:", err) 
			}

			fmt.Println("Arith: %d*%d=%d\n", args.A, args.B, reply)

			var quot Quotient
			err = client.Call("Arith.Divide", args, &quot)
			if err != nil {
				log.Fatal("arith error:", err)
			}
			fmt.Printf("Arith: %d/%d=%d remainder %d\n", args.A, args.B, quot.Quo, quot.Rem)
		}

	EX.5-A
		- The only difference in the client side code is that HTTP clients use DialHTTP, 
		  whereas TCP clients use Dial(TCP). 


***JSON RPC***
	
	- JSON RPC encodes data to JSON instead of gob

	EX.6 JSON RPC Sever Side Code

		package main

		import (
			"errors"
			"fmt"
			"net"
			"net/rpc"
			"net/rpc/jsonrpc"
			"os"
		)

		type Args struct {
			A, B int
		}

		type Quotient struct {
			Quo, Rem int 
		}

		type Arith int

		func (t *Arith) Multiply(args *Args, reply *int) error {
			*reply = args.A * args.B
			return nil 
		}

		func (t *Arith) Divide(args *Args, quo *Quotient) error {
			if args.B == 0 {
				return errors.New("divide by zero")
			}
			quo.Quo = args.A / args.B
			quo.Rem = args.A % args.B
			return nil 
		}

		func main() {
			
			arith := new(Arith)
			rpc.Register(arith)

			tcpAddr, err := net.ResolveTCPAddr("tcp", ":1234")
			checkError(err)

			listener, err := net.ListenTCP("tcp", tcpAddr)
			checkError(err)

			for {
				conn, err := listener.Accept()
				if err != nil {
					continue
				}
				jsonrpc.ServeConn(conn)
			}
		}

		// check error function 
		func checkError(err error) {
			if err != nil {
				fmt.Println("fatal error: ", err.Error())
				os.Exit(1)
			}
		}

	EX.6-A 
		- JSON RPC is based on TCP and doesn't support HTTP yet. 

	EX.7 JSON RPC Client Side

		package main 

		import ( 
			"fmt"
			"log"
			"net/rpc/jsonrpc"
			"os"
		)

		type Args struct {
			A, B int
		}

		type Quotient struct {
			Quo, Rem, int
		}

		func main() {
			if len(os.Args) != 2 {
				fmt.Println("Usage: ", os.Args[0], "server:port")
				log.Fatal(1)
			}
			service := os.Args[1]

			client, err := jsonrpc.Dial("tcp", service)
			if err != nil {
				log.Fatal("dialing:", err)
			}
			// synchronous call 
			args := Args{17, 8}
			var reply int
			err = client.Call("Arith.Multiply", args, &reply)
			if err != nil {
				log.Fatal("arith error", err)
			}
			fmt.Printf("arith: %d*%d=%d, args.A, args.B, reply)

			var quot Quotient
			err = client.Call("Arith.Divide", args, &quot)
			if err != nil {
				log.Fatal("arith error", err)
			}
			fmt.Println("Arith: %d/%d=%d remainder $d\n", args.A, args.B, quot.Quo, quot.Rem)
		}

	




































