#### How to Use Session ___________________________________________________________________

	- The basic principle behind sessions is that a server maintains information for every
	  single client, and clients rely on unique session id's to access this information. 

	Steps to Create a Session: 

		1) Create a unique session id
		2) open up a data storage space: normally we save sessions in memory, but you will
		   lose all session data if the system is accidentally interrupted. This can be a
		   very serious issue if web application deals with sensitive data. To fix this 
		   issue you can save session ID's to a DB-table. 
		3) Send the unique session id to the client

		- You have two ways to send session ids to clients: by cookies or URL rewrites

		1) Cookies: 
			- the server can easily use <Set-cookie> inside of the response header to send
			  a session id to a client, and a client can then use this cookie for future
			  requests. The base is to set session cookies with an expiry time of 0, which
			  means the cookie will be saved in memory and only deleted after users have
			  closed their browsers
		2) URL rewrite: 
			- append the session id as arguments in the URL for all pages. This ways seems
			  messy, but it's the best choice if clients have disabled cookies in their
			  browsers. 
	
	Key considerations in session management design: 

		- Global session manager
		- Keep session id unique 
		- Have on session for every users
		- Session storage in memory, file or database
		- Deal with expired sessions

	EX.1 Define a Global Session Manager

		type Manager struct { 
			cookieName		string	// private cookiename
			lock			sync.Mutex // protects session
			provider 		Provider
			maxlifetime		int64
		}

		func NewManager(provideName, cookieName string, maxlifetime int64) (*Manager, error) {
			provider, ok := provides[provideName]
			if !ok {
				return nil, fmt.Errorf("session: unknown provide %q (forgotten import?)",
										provideName)
			}
			return &Manager{provider: provider, cookieName: cookieName, maxlifetime:
							maxlifetime}, nil
		}

	EX.2 Create a global session manager in <main()> function: 

		var globalSessions *sessions.Manager
		// Then, initialize the session manager
		func init() {
			globalSessions = NewManager("memory", "gosessionid", 3600)
		}

	- You need to define a provider interface in order to prepresent the underlying structure
	  of the session manager. 

	EX.3 Provider for Session

		type Provider interface {
			SessionInit(sid string) (Session, error)
			SessionRead(sid string) (Session, error)
			SessionDestroy(side string) error
			SessionGC(maxLifeTime int64)
		}

	EX.3-A
		- <SessionInit>: implements the initialization of a session, and returns a new
						 session if it succeeds. 
		- <SessionRead>: returns a session represented by the corresponding sid. Creates
						 a new session and returns it if it does not already exist. 
		- <SessionDestroy>: given an sid, deletes the corresponding session
		- <SessionGC>: deletes expired session variables according to <maxLifeTime> 

	- Four Operations for sessions: set value, get value, delete value, and get current
									session id. 

	EX.4 Session Methods 

		type Session interface {
			Set(key, value interface{}) error // set session value
			Get(key interface{}) interface{}  // get session value
			Delete(key interface{}) error	  // delete session value
			SessionID() string				  // back current sessionID
		}

	EX.5 Session Register Function 

		var provides = make(map[string]Provider)

		// Register makes a session provider available by the provided name. 
		// if a register is called twice with the same name or if the driver is nil
		// it panics. 
		func Register(name string, provider Provider) {
			if provider == nil {
				panic("session: Register provider is nil")
			}
			if _, dup := provides[name]; dup {
				panic("session: Register called twice for provider " + name)
			}
			provides[name] = provider
		}

	EX.6 Unique Session ID's

		func (manager *Manager) sessionId() string {
			b := make([]byte, 32)
			if _, err := io.ReadFull(rand.Reader, b); err != nil {
				return ""
			}
			return base64.URLEncoding.EncodeToString(b)
		}

***Creating a Session***

	- The <SessionStart> function is for checking the existence of any sessions related
	  to the current user, and creating a new session if none if found. 

	EX.7 Creating a Session

		func (manager *Manager) SessionStart(w http.ResponseWriter, r *http.Response) (session Session) {
			manager.lock.Lock()
			defer manager.lock.Unlock()
			cookie, err := r.Cookie(manager.cookieName)
			if err != nil || cookie.Value == "" {
				side := manager.sessionId()
				session, _ = manager.provider.SessionInit(sid)
				cookie := http.Cookie{Name: manager.cookieName, Value: url.QueryEscape(sid), Path: "/", HttpOnly: true, MaxAge: int(manager.maxlifetime)}
				http.SetCookie(w, &cookie)
			} else {
				sid, _ := url.QueryUnexscape(cookie.Value)
				session, _ = manager.provider.SessionRead(sid)
			}
			return 
		}

	EX.8 Sessions for Login Operation 

		func login(w http.ResponseWriter, r *http.Request) {
			sess := globalSessions.SessionStart(w, r)
			r.ParseForm()
			if r.Method == "GET" {
				t, _ := template.ParseFiles("login.gtpl")
				w.Header().Set("Content-Type", "text/html")
				t.Execute(w, sess.Get("username"))
			} else {
				sess.Set("username", r.Form["username"])
				http>redirect(w, r, "/", 302)
			}
		}

***Operation value: set, get and delete***

	- The <SessionStart> function returns a variable that implements a session interface. 

	EX.9 Expanding basic operation of <session.Get('uid')>

		func count(w http.ResponseWrter, r *http.Response) {
			sess := globalSessions.SessionStart(w, r)
			createtime := sess.Get("createtime")
			if createtime == nil {
				sess.Set("createtime", time.Now().Unix())
			} else if (createtime.(int64) + 360) < (time.Now().Unix()) {
				globalSessions.SessionDestroy(w, r)
				sess = globalSessions.SessionStart(w, r)
			}
			ct := sess.Get("countnum")
			if ct == nil {
				sess.Set("contnum", 1)
			} else {
				sess.Set("countnum", (ct.(int) + 1))
			}
			t, _ := template.ParseFiles("count.gtpl")
			w.Header().Set("Content-Type", "text/html")
			t.Execute(w, sess.Get("countnum"))
		}

	EX.9-A
		- Because sessions have the concepts of an expiry time, we define the GC to update
		  the session's latest modify time. This way the GC will not delete sessions that
		  have expired but are still being used. 

***Reset Sessions***

	- When users logout, we need to delete the corresponding session. We've already used
	  the reset operation in above example - not let's take a look at the function body. 

	EX.10 Delete Corresponding Session

		func (manager *Manager) SessionDestroy(w http.ResponseWriter, r *http.Request) {
			cookie, err := r.Cookie(manager.cookieName)
			if err != nil || cookie.Value == "" {
				return 
			} else {
				manager.lock.Lock()
				defer manager.lock.Unlock()
				manager.provider.SessionDestroy(cookie.Value)
				expiration := time.Now()
				cookie := http.Cookie{Name: manager.cookieName, Path: "/", HttpOnly: true, 
									  Expires: expiration, MaxAge: -1}
				http.SetCookie(w, &cookie)
			}
		}

***Delete Sessions***

	- Session manager to delete a session. We need to start GC in the <main()> function: 

	EX.11 Delete

		func init() {
			go globalSessions.GC()
		}

		func (manager *Manager) GC() {
			manager.lock.Lock()
			defer manager.lock.Unlock()
			manager.provider.SessionGC(manager.maxlifetime)
			time.AfterFunc(time.Duration(manager.maxlifetime), func() { manager.GC() })
		}

	EX.11-A
		- We see that the GC makes full use of the timer function in the <time> package. It
		  automatically calls GC when the session times out, ensuring that all sessions are
		  usable during <maxLifeTime>. A similar solution can be used to count online users. 





































