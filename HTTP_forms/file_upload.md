#### File Upload ________________________________________________________________________

	- To upload files, you have to add the property <enctype> to the form that you want
	  to use for uploading photos. There are three possible values for this property. 

	EX.1 Possible Values: 

	1) application/x-www-form-urlencoded // transcode all characters before uploading
	2) multipart/form-data // no transcodign. You must use this value when your form
	                       // has a file upload controls
	3) text/plain // convert spaces to "+", but no transcoding for special characters. 

	EX.2 Upload API Route 

	http.HandleFunc("/upload", upload) // route function

	// upload logic
	func upload(w http.ResponseWritter, r *http.Request) {
		fmt.Println("method:", r.Method) 
		if r.Method == "GET" {
			crutime := time.Now().Unix()
			h := md5.New()
			io.WriteString(h, strconv.FormatInt(crutime, 10))
			token := fmt.Sprintf("%x", h.Sum(nil))

			t, _ := template.ParseFiles("upload.gtpl")
			t.Execute(w, token)
		} else {
			r.ParseMultipartForm(32 << 20)
			file, handler, err := r.FormFile("uploadfile")
			if err != nil {
				fmt.Println(err)
				return
			}
			defer file.Close()
			fmt.Fprintf(w, "%v", handler.Header)
			f, err := os.OpenFile("./test/"+handler.Filename, os.O_WRONLY|os.O_CREATE, 0666)
			if err != nil {
				fmt.Println(err)
				return
			}
			defer f.Close()
			io.Copy(f, file)
		}
	}

	EX.2-A
		- You need to call <r.ParseMultipartForm> for uploading files. The function
		  ParseMultipartForm takes the <maxMemory> argument. After you call 
		  <ParseMultipartform>, the file will be saved in the server memory with 
		  <maxMemory> size. If the file size is larger than <maxMemory>, the rest
		  of the data will be saved in a system temporary file. You can use <r.FormFil>
		  to get the file handle and use <io.Copy> to save to your file system.

		- You don't need to call <r.ParseForm> when you access other non-file fields in 
		  the form because Go will call it when it's necessary. Also calling 
		  <ParseMultipartForm> once is enough -multiple calls makes no difference. 

		- We use three steps for uploading files as follows: 
			1) Add <enctype="multipart/form-data"> to your form
			2) Call <r.ParseMultipartForm> on the server side to save the file either 
			   to memory or to a temporary file. 
			3) Call <r.Formfile> to get the file handle and save to the file system. 

	EX.3 File handler is the multipart.FileHeader -> struct 

		type FileHeader struct {
			Filename string
			Header textproto.MIMEHeader
			// contains filtered or unexported fields
		}

***Client Upload Files***

	EX.4 Impersonate a client form to upload files

		package main

		import (
			"bytes"
			"fmt"
			"io"
			"io/ioutil"
			"mime/multipart"
			"net/http"
			"os"
		)

		func postFile(filename string, targetUrl string) error {
			bodyBuf := &bytes.Buffer{}
			bodyWriter := multipart.NewWriter(bodyBuf)

			// this step is ver important !!
			fileWriter, err := bodyWriter.CreateFormFile("uploadfile", filename)
			if err != nil {
				fmt.Println("error writing to buffer")
				return err
			}

			// open file handle
			fh, err := os.Open(filename)
			if err != nil {
				fmt.Println("error opening file")
				return err
			}
			defer fh.Close()

			// iocopy
			_, err = io.Copy(fileWriter, fh)
			if err != nil {
				return err
			}

			contentType := bodyWriter.FormDataContentType()
			bodyWriter.Close()

			resp, err := http.Post(targetUrl, contentType, bodyBuf)
			if err != nil {
				return err
			}
			defer resp.Body.Close()
			resp_body, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				return err
			}
			fmt.Println(resp.Status)
			fmt.Println(string(resp_body))
			return nil
		}

		// sample usage 
		func main() {
			target_url := "http://localhost:9090/upload"
			filename := "./example.pdf"
			postFile(filename, target_url)
		}

	EX.4-A
		- It uses <multipart.Write> to write file into cache and sends them to the server
		  through the POST method. 
		- If you have other fields to write into data, like username, call 
		  <multipart.WriteField> as needed. 







